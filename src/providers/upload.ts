import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';

/*
  Generated class for the Upload provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Upload {
  progressObserver:any;
  progress$:any;
  progress:any;
  constructor(public http: Http) {
    this.progress$ = Observable.create(observer => {
        this.progressObserver = observer
    }).share();
  }

  image(){
    return Observable.create(observer => {
      let formData: FormData = new FormData(),
          xhr: XMLHttpRequest = new XMLHttpRequest(),
          res;
      res = {
        response: JSON.parse(xhr.response),
        progress: 0,
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
              observer.next(JSON.parse(xhr.response));
              observer.complete();
          } else {
              observer.error(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.progress = Math.round(event.loaded / event.total * 100);

        this.progressObserver.next(this.progress);
      };

      xhr.open('POST', 'http://localhost:3000/upload', true);
      xhr.send(formData);
    });
  }

}
