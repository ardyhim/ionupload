import { Component, NgZone } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loadProgress:any = 0;
  constructor(public navCtrl: NavController, private zone: NgZone) {
  }

  upload(e){
    let file = e.target.files[0];
    let formData: FormData = new FormData();
    let xhr: XMLHttpRequest = new XMLHttpRequest();
    xhr.setRequestHeader('authorization', window.localStorage.getItem('token'));
    formData.append("file", file);
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          console.log(JSON.parse(xhr.response))
        } else {
          console.log(JSON.parse(xhr.response))
        }
      }
    };

    xhr.upload.onprogress = (event) => {
      this.zone.run(()=>{
        this.loadProgress = Math.round(event.loaded / event.total * 100);
      })
    };

    xhr.open('POST', 'http://localhost:3000/upload', true);
    xhr.send(formData);
  }

}
